import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('local_control_room')


def test_epics_env_variables(host):
    cmd = host.run('source /etc/profile && env')
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-epics-base-activate-env":
        assert "EPICS_CA_ADDR_LIST" not in cmd.stdout
        assert "EPICS_CA_AUTO_ADDR_LIST" not in cmd.stdout
    elif host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-epics-base-epics-ca":
        assert "EPICS_CA_ADDR_LIST=192.168.1.255 192.168.2.255" in cmd.stdout
        assert "EPICS_CA_AUTO_ADDR_LIST=NO" in cmd.stdout


def test_epics_base_env_created(host):
    assert host.file('/opt/conda/envs/epics/epics/lib/linux-x86_64/libca.so').exists
